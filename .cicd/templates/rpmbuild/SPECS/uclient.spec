Name: uclient
Version: RPM_VERSION
Release: RPM_RELEASE
Summary: Contains the UManager client (uclient) 
License: MIT
Requires: mysql
Provides: uclient

%description
Contains the UManager client (uclient)

%build
pyinstaller --onefile %{_sourcedir}/uclient-bin.spec

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_sysconfdir}/uclient
install -m 755 %(_builddir)dist/uclient %{buildroot}%{_bindir}/uclient
install -m 644 %{_sourcedir}/.cicd/templates/config.ini %{buildroot}%{_sysconfdir}/uclient/config.ini
install -m 644 %{_sourcedir}/.cicd/templates/uclientd.service %{buildroot}%{_unitdir}/uclientd.service

%post
%systemd_post uclientd.service

%files
%{_bindir}/uclient
%config %{_sysconfdir}/uclient
%{_unitdir}/uclientd.service
