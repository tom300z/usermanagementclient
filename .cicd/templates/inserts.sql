use Umanager;
insert into Umanager.user (public_key, private_key, login, `password`, state)
values ("public", "private", "tom300z", "pass", "ENABLED");
insert into Umanager.user (public_key, private_key, login, `password`, `state`)
values ("public", "private", "admin", "supersecret", "ENABLED");
insert into Umanager.user (public_key, private_key, login, `password`, `state`)
values ("public", "private", "anonymous", "WeAreLegion", "DELETED");
insert into Umanager.host (hostid, state)
values ("srv1.test.localhost", "ENABLED");
insert into Umanager.group (name, state)
values ("admingroup", "ENABLED");
insert into Umanager.group (name, state)
values ("Developer", "ENABLED");
insert into Umanager.usergroups (userLogin, groupName)
values ("tom300z", "Developer");
insert into Umanager.usergroups (userLogin, groupName)
values ("admin", "admingroup");
insert into Umanager.usergroups (userLogin, groupName)
values ("anonymous", "admingroup");
insert into Umanager.usergroups (userLogin, groupName)
values ("tom300z", "tom300z");
insert into Umanager.usergroups (userLogin, groupName)
values ("admin", "admin");
insert into Umanager.usergroups (userLogin, groupName)
values ("anonymous", "anonymous");

CREATE USER IF NOT EXISTS 'client'@"172.17.0.3" IDENTIFIED BY "";
GRANT SELECT,SHOW VIEW ON *.* TO 'client'@"172.17.0.3" IDENTIFIED BY "";