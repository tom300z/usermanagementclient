import argparse
import time
from program.meth import *
from program.rollout import rollout

# Checks if the program is running as root and throws an error if that's not the case.
if os.geteuid() != 0:
    log.critical("This script must be run as root (uid 0)")

# Initializes The argument parser.
parser = argparse.ArgumentParser()
parser.add_argument("-r", "--rollout", action="store_true", help="Apply new users, groups and relationships")
parser.add_argument("-v", "--verbose", action="store_true", help="Display debug information")
args = parser.parse_args()

# Sets the logging level to debug if the "verbose" argument is given.
if args.verbose:
    log.setLevel(logging.DEBUG)
    log.debug("Starting in verbose mode")

# Runs the rollout script if the "rollout" argument is given.
if args.rollout:
    rollout()
else:
    log.info("Starting the uclient service")
    while True:
        rollout()
        time.sleep(10)
