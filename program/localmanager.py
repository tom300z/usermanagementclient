import program.dbmanager as db
from program.meth import *

class List:
    def Users(self):
        """Returns a list of all local users."""
        userlist = []
        with open(conf["Local"]["passwd_file"], "r")as userfile:
            for line in userfile:
                userlist.append(line.split(":")[0])
        return userlist

    def Groups(self):
        """Returns a list of all local groups."""
        grouplist = []
        with open(conf["Local"]["group_file"], "r")as groupfile:
            for line in groupfile:
                grouplist.append(line.split(":")[0])
        return grouplist



class User:
    """This class takes a dbmanager.User object and creates a localmanager.User object from it.
     This Class is used to manage the local user corresponding to its "self.login" attribute."""
    def __init__(self, new):
        """Initializes the class, and sets all of its attributes"""
        self.new = new
        if self.new.indb(all=True):
            self.login = self.new.login
            if self.onhost():
                with open(conf["Local"]["passwd_file"], "r") as userfile:
                    for line in userfile.read().splitlines():
                        if line.split(":")[0] == self.new.login:
                            self.passwddata = line.split(":")

                with open(conf["Local"]["shadow_file"], "r") as userfile:
                    for line in userfile.read().splitlines():
                        if line.split(":")[0] == self.new.login:
                            self.shadowdata = line.split(":")

                self.homedir = self.passwddata[5]
                self.shell = self.passwddata[6]
                self.authkeypath = self.homedir + "/.ssh/authorized_keys"
                self.idrsapath = self.homedir + "/.ssh/id_rsa"

                if os.path.isfile(self.authkeypath):
                    self.public_key = open(self.authkeypath, "r").read()
                else:
                    self.public_key = ""

                if os.path.isfile(self.idrsapath):
                    self.private_key = open(self.idrsapath, "r").read()
                else:
                    self.private_key = ""

                self._password = self.checkpassword()

                self.groups = self.grouplist()

                if self.shadowdata[7] == "0":
                    self.state = "DISABLED"
                elif self.shadowdata[7] == "":
                    self.state = "ENABLED"
            else:
                log.debug("User \"{}\" does not exist locally".format(self.new.login))
                self.homedir = "/home/" + self.login
                self.authkeypath = self.homedir + "/.ssh/authorized_keys"
                self.idrsapath = self.homedir + "/.ssh/id_rsa"

                self.public_key = None
                self.private_key = None
                self._password = None
                self.groups = [None]
                self.state = "DELETED"
        else:
            log.warning("User \"{}\" does not exist in the database".format(self.new.login))

    def enable(self):
        """Makes sure that the user exists locally, and all of it's attributes are rolled out to the
        local user."""

        # Creates a now local user if it does not exist
        if not self.onhost():
            log.info("Creating new local user \"{}\"".format(self.login))
            run("useradd {}".format(self.new.login))

        # Enables the local user.
        run("sudo chage -E-1 {}".format(self.login))

        # Checks whether the locally configured public key matches the one in the database and updates the local one if
        # required.
        if self.new.public_key + "=" != self.public_key:
            log.info("Rolling out new public key of local user \"{}\"".format(self.login))
            os.makedirs(self.homedir + "/.ssh", exist_ok=True)
            file = open(self.authkeypath, "w")
            file.write(self.new.public_key + "=")
            file.close()

        # Checks whether the locally configured private key matches the one in the database and updates the local one
        # if required.
        if self.new.private_key + "=" != self.private_key:
            log.info("Rolling out new private key of local user \"{}\"".format(self.login))
            os.makedirs(self.homedir + "/.ssh", exist_ok=True)
            file = open(self.idrsapath, "w")
            file.write(self.new.private_key + "=")
            file.close()

        # Checks whether the locally configured password is emptyand updates the local one if it's not.
        if self.new._password != self._password:
            log.info("Rolling out password of local user \"{}\"".format(self.login))
            run("sudo chage -m0 {}".format(self.login))

            if self.new._password == "":
                run("passwd -d {user}".format(user=self.new.login))
            else:
                run("echo \"{pas}\" | passwd --stdin {user}".format(pas=self.new._password, user=self.new.login))

        # Sets the minimal time period until a password change is allowed to a high value, so that the managed user
        # cant change it's password per host.
        run("sudo chage -m999999 {}".format(self.login))

        # Checks if the local user has the right group memberships and updates them if required,
        # if a group does not exist locally, it will be created.
        if sorted(self.new.groups) != sorted(self.groups):
            log.info("Rolling out new group memberships of local user \"{}\" ".format(self.login))
            localgroups = List().Groups()
            for group in self.new.groups:
                groupdbobj = db.Group(name=group)
                if groupdbobj.state == "ENABLED":
                    if group not in localgroups:
                        log.info("Adding local group \"{}\"".format(group))
                        groupobj = Group(groupdbobj, name=group)
                        groupobj.enable()
                    run("usermod -G {} {}".format(",".join(self.new.groups), self.login))


        log.debug("Local user \"{}\" is now enabled".format(self.login))

    def disable(self):
        """Locks the local user."""
        if not self.onhost():
            log.info("Creating new local user \"{}\"".format(self.login))
            run("useradd {}".format(self.new.login))
        log.info("Disabling local user \"{}\"".format(self.login))
        run("sudo chage -E0 {}".format(self.login))
        log.debug("Local user \"{}\" is now disabled".format(self.login))

    def delete(self):
        """Deletes the Local User and, depending on the configured parameters, it's home directory."""
        if self.onhost():
            log.info("Deleting local user \"{}\"".format(self.login))
            run("userdel {}".format(self.login))
            if conf["Local"]["delete_home_dir"]:
                log.debug("Deleting the home directory of local user \"{}\"".format(self.login))
                run("rm -rf /home/{}".format(self.login))
        log.debug("Local user \"{}\" is now deleted".format(self.login))

    def grouplist(self):
        """Returns a list of all groups that the local user is currently a part of."""
        return run("groups {}".format(self.login)).split(" : ")[1].split(" ")

    def onhost(self):
        """Checks whether the user exists on this host and returns the according boolean value."""
        if self.new == None:
            return False
        elif self.new.login in List().Users():
            return True
        else:
            return False

    def infodict(self):
        """Returns a dictionary with the most important attributes of this object."""
        datadict = {}
        if self.onhost():
            datadict["public_key"] = self.public_key
            datadict["private_key"] = self.private_key
            datadict["login"] = self.login
            datadict["password"] = self._password
            datadict["state"] = self.state
            datadict["groups"] = self.groups
        else:
            datadict = None
            log.info("User \"{}\" does not exist locally".format(self.login))
        return datadict

    def checkpassword(self):
        """Checks whether the password of the local user is empty or not
        and returns an empty string if that's the case."""
        password = None
        if self.shadowdata[1] == "":
            password = ""
        return password


class Group(object):
    """This class takes a dbmanager.Group object and creates a localmanager.Group object from it.
         This Class is used to manage the local group corresponding to its "self.name" attribute."""
    def __init__(self, new, name=None):
        """Initializes the class, and sets all of its attributes."""
        self.new = new
        if self.new.indb(all=True):
            self.name = self.new.name
            if self.onhost():
                with open(conf["Local"]["group_file"], "r") as groupfile:
                    for line in groupfile.read().splitlines():
                        if line.split(":")[0] == self.new.name:
                            self.groupdata = line.split(":")

                self.state = "ENABLED"
            else:
                log.debug("Group \"{}\" does not exist locally".format(self.new.name))
                self.users = [None]
                self.state = "DELETED"

            self.members = self.userlist()

        else:
            log.warning("Group \"{}\" does not exist in the database".format(self.new.name))
            if name != None:
                self.name = name

    def enable(self):
        """Makes sure that the group exists locally."""
        if not self.onhost():
            log.info("Creating new local group \"{}\"".format(self.name))
            run("groupadd {}".format(self.name))
        log.debug("Local group \"{}\" is now enabled".format(self.name))

    def delete(self):
        """Deletes the local group."""
        if self.onhost():
            log.info("Deleting local Group \"{}\"".format(self.name))
            run("groupdel {}".format(self.name))
        log.debug("Local group \"{}\" is now deleted".format(self.name))

    def disable(self):
        """Disables the local group."""
        if self.onhost():
            log.info("Disabling local Group \"{}\"".format(self.name))
            run("groupdel {}".format(self.name))
        log.debug("Local group \"{}\" is now disabled".format(self.name))

    def userlist(self):
        """Returns a list of all members of the local group."""
        if self.onhost():
            return self.groupdata[3].split(",")
        else:
            return []

    def onhost(self):
        """Checks whether the user exists on this host and returns the according boolean value."""
        if self.new == None:
            return False
        elif self.new.name in List().Groups():
            return True
        else:
            return False

    def infodict(self):
        """Returns a dictionary with the most important attributes of this object."""
        datadict = {}
        if self.onhost():
            datadict["name"] = self.name
            datadict["state"] = self.state
        else:
            datadict = None
            log.info("User \"{}\" does not exist locally".format(self.name))
        return datadict
