import os
import re
import sys
import string
import random
import logging
import platform
import subprocess
import configparser

# Import a MySQL connector, both MySQLdb and pymysql are supported.
try:
    import pymysql as MySQLdb

    MySQLdb.install_as_MySQLdb()
except ImportError:
    import MySQLdb
    import MySQLdb._exceptions


def fullpath(path):
    """Return the full path of a relative path."""
    if str(path)[0] == "/":
        return path
    else:
        return os.path.join(os.path.dirname(os.path.dirname(__file__)), path)


def exitprogramm():
    """Exit the program."""
    exit()

def randomstring(length=10):
    """Generate a random string of a fixed length."""
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

def printdict(data):
    """Brings a dict into human readable form and prints it out."""
    for key in data:
        print("{:<15}{:<20}".format(key + ":", str(data[key])))

def run(cmd):
    """Runs the given command in a shell and returns the output."""
    process = subprocess.Popen(cmd.split(" "), stdout=subprocess.PIPE)
    output = process.communicate()[0].decode("UTF-8").rstrip()
    return output


# Reads the default config files.
conf = configparser.ConfigParser()
conf.read(fullpath("default_config.ini"))
conf.read(fullpath(conf["Misc"]["config_file"]))

# Creates and configures a logger object.
log = logging.getLogger()
log.setLevel(logging.INFO)
fileformatter = logging.Formatter("[%(asctime)s] [%(levelname)-7s] - %(message)s", "%Y-%m-%d %H:%M:%S")
streamformatter = logging.Formatter("%(levelname)s: %(message)s")
fh = logging.FileHandler(fullpath(conf["Logging"]["logfile"]))
ch = logging.StreamHandler()
fh.setFormatter(fileformatter)
ch.setFormatter(streamformatter)
if conf["Logging"]["file_logging"]:
    log.addHandler(fh)
if conf["Logging"]["console_logging"]:
    log.addHandler(ch)


class ShutdownHandler(logging.Handler):
    """A simple logging handle that exits the program when called."""
    def emit(self, record):
        print(" --- Fatal error, program aborted --- ", file=sys.stderr)
        logging.shutdown()
        exit(1)


log.addHandler(ShutdownHandler(level=logging.ERROR))


# Reads the Database credentials from the configuration.
dbhost = conf["Database"]["host"]
dbuser = conf["Database"]["user"]
dbpassword = conf["Database"]["password"]
dbdatabase = "Umanager"

# Tries to connect to the database server and throws an ecxeption if something doesn't work.
dbcon = MySQLdb.connect(host=dbhost, user=dbuser, passwd=dbpassword)
dbcur = dbcon.cursor()
dbcur.execute("""SHOW DATABASES""")

# Tries to use the configured database.
dbcur.execute("""USE {}""".format(dbdatabase))

