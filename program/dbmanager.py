from program.meth import *


class List(object):
    def __init__(self):
        pass

    def users(self, all=False):
        """Returns a list of users in the database, if the argument "all" is True the list will also include deleted
        users,"""
        if all:
            dbcur.execute("""SELECT login FROM Umanager.user""")
        else:
            dbcur.execute("""SELECT login FROM Umanager.user WHERE state!="{}" """.format("DELETED"))
        userlist = []
        data = dbcur.fetchall()
        for i in data:
            userlist.append(i[0])
        return userlist

    def groups(self, all=False):
        """Returns a list of groups in the database, if the argument "all" is True the list will also include deleted
        groups,"""
        if all:
            dbcur.execute("""SELECT name FROM Umanager.group""")
        else:
            dbcur.execute("""SELECT name FROM Umanager.group WHERE state!="{}" """.format("DELETED"))
        grouplist = []
        data = dbcur.fetchall()
        for i in data:
            grouplist.append(i[0])
        return grouplist

    def hosts(self, all=False):
        """Returns a list of users in the database, if the argument "all" is True the list will also include deleted
        users,"""
        if all:
            dbcur.execute("""SELECT hostid FROM Umanager.host""")
        else:
            dbcur.execute("""SELECT hostid FROM Umanager.host WHERE state!="{}" """.format("DELETED"))
        hostlist = []
        data = dbcur.fetchall()
        for i in data:
            hostlist.append(i[0])
        return hostlist


class User(object):
    def __init__(self, login=None):
        """Initializes the class, and sets all of its attributes"""
        self.login = login
        if self.indb(all=True):
            dbcur.execute("""SELECT * FROM user WHERE login="{}";""".format(self.login))
            data = dbcur.fetchall()[0]
            self.public_key = data[1]
            self.private_key = data[2]
            self._password = data[4]
            if data[5].upper() in ["CREATED", "ENABLED", "DISABLED", "DELETED"]:
                self.state = data[5].upper()
            else:
                log.warning("Invalid state \"{}\" setting it to DISABLED".format(data[4]))
                self.state = "DISABLED"
        else:
            log.debug("User {} not found".format(self.login))
            self.public_key = ""
            self.private_key = ""
            self._password = ""
            self.state = "ENABLED"

        self.groups = self.grouplist()

    def indb(self, all=False):
        """Checks whether the user exists in the database and returns the according boolean value."""
        if self.login in List().users(all=all):
            return True
        else:
            return False

    def grouplist(self):
        """Returns a list of all groups that the user is currently a part of."""
        grouplist = []
        dbcur.execute("""SELECT * FROM `Umanager`.`usergroups` WHERE userLogin="{}" """.format(self.login))
        data = dbcur.fetchall()
        for row in data:
            groupName = row[2]
            grouplist.append(groupName)
        return grouplist

    def infodict(self):
        """Returns a dictionary with the most important attributes of this object."""
        datadict = {}
        if self.indb():
            datadict["public_key"] = self.public_key
            datadict["private_key"] = self.private_key
            datadict["login"] = self.login
            datadict["password"] = self._password
            datadict["state"] = self.state
            datadict["groups"] = self.grouplist()
        else:
            datadict = None
            log.info("User \"{}\" does not exist in the database".format(self.login))
        return datadict


class Group(object):
    def __init__(self, name=None):
        """Initializes the class, and sets all of its attributes"""
        self.name = name
        if self.indb(all=True):
            dbcur.execute("""SELECT * FROM `Umanager`.`group` WHERE name="{}";""".format(self.name))
            data = dbcur.fetchall()[0]
            if data[1].upper() in ["CREATED", "ENABLED", "DISABLED", "DELETED"]:
                self.state = data[1]
            else:
                log.warning("Invalid state \"{}\" setting it to DISABLED".format(data[4]))
                self.state = "DISABLED"
        else:
            log.debug("Group {} not found in the database".format(self.name))
            self.state = "ENABLED"

    def indb(self, all=False):
        """Checks whether the group exists in the database and returns the according boolean value."""
        if self.name in List().groups(all=all):
            return True
        else:
            return False

    def memberlist(self):
        """Returns a list of all members of the group."""
        memberlist = []
        dbcur.execute("""SELECT * FROM `Umanager`.`usergroups` WHERE groupName="{}" """.format(self.name))
        data = dbcur.fetchall()
        for row in data:
            userName = row[1]
            memberlist.append(userName)
        return memberlist

    def infodict(self):
        """Returns a dictionary with the most important attributes of this object."""
        datadict = {}
        if self.indb():
            datadict["name"] = self.name
            datadict["members"] = self.memberlist()
        else:
            datadict = None
            log.info("Group \"{}\" does not exist in the database".format(self.name))
        return datadict


class Host(object):
    def __init__(self, hostid=None):
        """Initializes the class, and sets all of its attributes"""
        self.hostid = hostid
        if self.indb():
            dbcur.execute("""SELECT * FROM host WHERE hostid="{}";""".format(self.hostid))
            data = dbcur.fetchall()[0]
            self.hostname = data[2]
            self.protocol = data[3]
            self.port = data[4]
            if data[5].upper() in ["CREATED", "ENABLED", "DISABLED", "DELETED"]:
                self.state = data[5]
            else:
                log.warning("Invalid state \"{}\" setting it to DISABLED".format(data[5]))
                self.state = "DISABLED"
        else:
            log.debug("Host {} not found in the database".format(self.hostid))
            self.hostname = ""
            self.protocol = "ssh"
            self.port = "22"
            self.state = "ENABLED"

    def indb(self, all=False):
        """Checks whether the Host exists in the database and returns the according boolean value."""
        if self.hostid in List().hosts(all=all):
            return True
        else:
            return False

    def infodict(self):
        """Returns a dictionary with the most important attributes of this object."""
        datadict = {}
        if self.indb():
            datadict["hostid"] = self.hostid
            datadict["hostname"] = self.hostname
            datadict["protocol"] = self.protocol
            datadict["port"] = self.port
            datadict["state"] = self.state
        else:
            datadict = None
            log.info("Host \"{}\" does not exist in the database".format(self.hostid))
        return datadict
