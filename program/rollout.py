from program.meth import *
import program.dbmanager as db
import program.localmanager as local

def rollout():
    # Rolls out the users from the database.
    log.info("Rolling out Users...")
    for user in db.List().users(all=True):
        dbuser = db.User(login=user)
        localuser = local.User(dbuser)
        if dbuser.state == "ENABLED":
            localuser.enable()
        elif dbuser.state == "DISABLED":
            localuser.disable()
        elif dbuser.state == "DELETED":
            localuser.delete()


    # Rolls out the groups from the database.
    log.info("Rolling out Groups...")
    for group in db.List().groups(all=True):
        dbgroup = db.Group(name=group)
        localgroup = local.Group(dbgroup)
        if dbgroup.state == "ENABLED":
            localgroup.enable()
        elif dbgroup.state == "DISABLED":
            localgroup.disable()
        elif dbgroup.state == "DELETED":
            localgroup.delete()
